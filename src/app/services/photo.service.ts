import { Injectable } from '@angular/core';
import { Plugins, CameraResultType, Capacitor, FilesystemDirectory, 
  CameraPhoto, CameraSource } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { DBPhoto } from '../shared/db-photo';
import { DBPhotosServices } from './db-photos.service';


  

const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  public photos: DBPhoto[] = [];
  private PHOTO_STORAGE: string = "photos";
  private platform: Platform;


  constructor(platform: Platform, private dbPhotoService: DBPhotosServices) {
    this.platform = platform;
  }

  public async addNewToGallery() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri, 
      source: CameraSource.Camera, 
      quality: 100 
    });

    const savedImageFile = await this.savePicture(capturedPhoto);
    // this.photos.unshift(savedImageFile);

    Storage.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos)
    });
  }

  private async savePicture(cameraPhoto: CameraPhoto) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(cameraPhoto);
    const fileName = new Date().getTime() + '.jpeg';
    console.log("saving photo!");
    console.log(base64Data);

    this.saveToDatabase(fileName, base64Data);
  }

  private saveToDatabase(name: string, file: string) {
    let photo = new DBPhoto();
    photo.name = name;
    photo.file = file;
    this.dbPhotoService.createPhoto(photo).then(res => {
      console.log(res);
    })
    .catch(error => console.log(error));
  }

  private async readAsBase64(cameraPhoto: CameraPhoto) {
    // "hybrid" will detect Cordova or Capacitor
    if (this.platform.is('hybrid')) {
      // Read the file into base64 format
      const file = await Filesystem.readFile({
        path: cameraPhoto.path
      });

      return file.data;
    }
    else {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(cameraPhoto.webPath);
      const blob = await response.blob();

      return await this.convertBlobToBase64(blob) as string;
    }  
  }
  
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  public async loadSaved() {
    let photoList = this.dbPhotoService.getPhotoList();
    photoList.snapshotChanges().subscribe(res => {
      this.photos = [];
      res.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;
        console.log(a);
        this.photos.push(a as DBPhoto);
      })
    })
  }

}

export interface Photo {
  filepath: string;
  webviewPath: string;
}
