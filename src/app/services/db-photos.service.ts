import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { DBPhoto } from '../shared/db-photo';

@Injectable({
  providedIn: 'root'
})

export class DBPhotosServices {
  photoListRef: AngularFireList<any>;
  photoRef: AngularFireObject<any>;

  constructor(private db: AngularFireDatabase) {
   }

  // Create
  createPhoto(photo: DBPhoto) {
    this.photoListRef = this.db.list('/photo');
    console.log("made it here? "+ photo.name);
    return this.photoListRef.push({
      name: photo.name,
      file: photo.file
    })
  }

  // Get Single
  getPhoto(id: string) {
    this.photoRef = this.db.object('/photo/' + id);
    return this.photoRef;
  }

  // Get List
  getPhotoList() {
    this.photoListRef = this.db.list('/photo');
    return this.photoListRef;
  }

  // Delete
  deletePhoto(id: string) {
    this.photoRef = this.db.object('/photo/' + id);
    this.photoRef.remove();
  }
}
