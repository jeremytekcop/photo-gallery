import { TestBed } from '@angular/core/testing';

import { DbPhotosService } from './db-photos.service';

describe('DbPhotosService', () => {
  let service: DbPhotosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DbPhotosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
