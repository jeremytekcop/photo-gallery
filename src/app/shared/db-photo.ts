export class DBPhoto {
    $key: string;
    name: string;
    file: string;
}