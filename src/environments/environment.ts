// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCMPKWbYtONjAGist0MqsR_CnkHEplzsKI",
    authDomain: "photo-gallery-51827.firebaseapp.com",
    databaseURL: "https://photo-gallery-51827-default-rtdb.firebaseio.com",
    projectId: "photo-gallery-51827",
    storageBucket: "photo-gallery-51827.appspot.com",
    messagingSenderId: "1075210557755",
    appId: "1:1075210557755:web:21bd8363569f04d6787f1e",
    measurementId: "G-ZENCLV2Y15"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
